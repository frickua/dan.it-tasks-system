package dan.it.controller.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import dan.it.dto.TaskCheckRequest;
import dan.it.entity.TaskEntity;
import dan.it.entity.TaskResultEntity;
import dan.it.service.ITaskService;

@RestController
public class TasksController {

	@Autowired
	private ITaskService taskService;

	@GetMapping
	@RequestMapping(path = "/tasks")
	public List<TaskEntity> getTasks() {
		return taskService.getTasks();
	}

	@PostMapping
	@RequestMapping("/tasks/{id}/check")
	public TaskResultEntity checkTask(@PathVariable("id") Long taskId, @RequestBody TaskCheckRequest taskCheckRequest) {

		return taskService.chekTask(taskId, taskCheckRequest);
	}
}
