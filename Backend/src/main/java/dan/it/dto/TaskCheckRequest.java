package dan.it.dto;

import lombok.Data;

@Data
public class TaskCheckRequest {
	String code;
	String packageName;
	String fileName;
}
