package dan.it.entity;

import dan.it.Language;
import lombok.Data;

@Data
public class TaskEntity {

	private Long taskId;

	private String taskName;

	private String taskDescription;

	private Language language;

}
