package dan.it.entity;

import lombok.Data;

@Data
public class TaskResultEntity {

	private TaskEntity task;
	private String runOutput;
	private String compileOutput;
	private Exception runException;
	private Exception  compileException;
}
