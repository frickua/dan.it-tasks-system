package dan.it.repository;

import java.util.List;

import dan.it.entity.TaskEntity;

public interface ITaskRepository {

	List<TaskEntity> getAllTasks();

	TaskEntity getTaskById(Long taskId);
}
