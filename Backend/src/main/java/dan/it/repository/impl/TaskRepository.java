package dan.it.repository.impl;

import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

import dan.it.Language;
import dan.it.entity.TaskEntity;
import dan.it.repository.ITaskRepository;

@Repository
public class TaskRepository implements ITaskRepository {

	@Override
	public List<TaskEntity> getAllTasks() {
		List<TaskEntity> tasks = new ArrayList<>();
		TaskEntity task = new TaskEntity();
		task.setTaskId(1L);
		task.setLanguage(Language.JAVA);
		task.setTaskName("Сортировка строк");
		task.setTaskDescription("Необходимо отсортировать масив в порядке возрастания");
		tasks.add(task);

		task = new TaskEntity();
		task.setTaskId(2L);
		task.setLanguage(Language.JAVA);
		task.setTaskName("Поиск самого короткого слова");
		task.setTaskDescription("Необходимо найти самое короткое слово в масиве");
		tasks.add(task);

		task = new TaskEntity();
		task.setTaskId(3L);
		task.setLanguage(Language.JAVA);
		task.setTaskName("Решение системы уровнений");
		task.setTaskDescription("Написать программу для решения квадратных уровнений");
		tasks.add(task);
		return tasks;
	}

	@Override
	public TaskEntity getTaskById(Long taskId) {
		return getAllTasks().stream().filter(task -> task.getTaskId().equals(taskId)).findAny().get();
	}

}
