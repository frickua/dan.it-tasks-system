package dan.it.service;

import java.io.IOException;

import dan.it.dto.TaskCheckRequest;
import dan.it.entity.TaskResultEntity;

public interface IJavaTaskExecutionService {

	TaskResultEntity runClass(TaskCheckRequest taskCheckRequest, String[] args) throws IOException;
}
