package dan.it.service;

import java.util.List;

import dan.it.dto.TaskCheckRequest;
import dan.it.entity.TaskEntity;
import dan.it.entity.TaskResultEntity;

public interface ITaskService {

	List<TaskEntity> getTasks();

	TaskResultEntity chekTask(Long taskId, TaskCheckRequest taskCheckRequest);
}
