package dan.it.service.impl;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecuteResultHandler;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteWatchdog;
import org.apache.commons.exec.Executor;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;

import dan.it.dto.TaskCheckRequest;
import dan.it.entity.TaskResultEntity;
import dan.it.service.IJavaTaskExecutionService;

@Component
public class JavaTaskExecutionService implements IJavaTaskExecutionService {

	private Logger log = Logger.getLogger(JavaTaskExecutionService.class.getName());

	@Override
	public TaskResultEntity runClass(TaskCheckRequest taskCheckRequest, String[] args) throws IOException {
		log.info("Started java compilation");
		Path tmpDir = Files.createTempDirectory("java-tsk");
		File javaFile = new File(tmpDir.toFile(), taskCheckRequest.getFileName());

		FileUtils.writeStringToFile(javaFile, taskCheckRequest.getCode(), StandardCharsets.UTF_8);

		CommandLine cmdLine = new CommandLine("javac");

//		cmdLine.addArgument("-verbose ");
		cmdLine.addArgument("-d ");
		cmdLine.addArgument("${directory}");
		cmdLine.addArgument("${file}");
		HashMap map = new HashMap();
		map.put("file", javaFile);
		map.put("directory", javaFile.getParentFile());
		cmdLine.setSubstitutionMap(map);

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

		ExecuteWatchdog watchdog = new ExecuteWatchdog(60*1000);
		Executor executor = new DefaultExecutor();
		executor.setExitValue(0);
		executor.setWatchdog(watchdog);
		executor.setWorkingDirectory(javaFile.getParentFile());
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		executor.setStreamHandler(new PumpStreamHandler(out));
		TaskResultEntity taskResult = new TaskResultEntity();
		File classFile = null;
		try {
			executor.execute(cmdLine, resultHandler);
			resultHandler.waitFor();
			taskResult.setCompileOutput(out.toString("UTF-8"));
			taskResult.setCompileException(resultHandler.getException());
			classFile = new File(javaFile.getAbsolutePath().replace(".java", ".class"));
			runTask(taskResult, javaFile.getParentFile(), fullClassName(taskCheckRequest), args);

		} catch (IOException | InterruptedException e) {
			taskResult.setCompileException(e);
			taskResult.setCompileOutput(stackToString(e));
		} finally {
			try {
				FileUtils.deleteDirectory(tmpDir.toFile());
			} catch (Throwable th) {
				log.log(Level.SEVERE, "Unable to delete temporally files.", th);
			}
		}

		return taskResult;
	}

	private void runTask(TaskResultEntity taskResult, File workingDirectory, String mainClass, String[] args) {
		log.info("Started task execution. " + mainClass);
		CommandLine cmdLine = new CommandLine("java");
		if (args != null) {
			for (String arg : args) {
				cmdLine.addArgument(arg);
			}
		}
//		cmdLine.addArgument("-verbose ");
		cmdLine.addArgument("${file}");
		HashMap map = new HashMap();
		map.put("file", mainClass);
		cmdLine.setSubstitutionMap(map);

		DefaultExecuteResultHandler resultHandler = new DefaultExecuteResultHandler();

		ExecuteWatchdog watchdog = new ExecuteWatchdog(15*1000);
		Executor executor = new DefaultExecutor();
		executor.setExitValue(0);
		executor.setWatchdog(watchdog);
		executor.setWorkingDirectory(workingDirectory);
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		executor.setStreamHandler(new PumpStreamHandler(out));
		try {
			executor.execute(cmdLine, resultHandler);
			resultHandler.waitFor();
			String runOutput = out.toString("UTF-8");
			Exception runException = resultHandler.getException();
			if (watchdog.killedProcess()) {
				Exception timeoutEx = new TimeoutException("Your code exceeded the time limit");
				if (runException != null) {
					runException.addSuppressed(timeoutEx);
				} else {
					runException = timeoutEx;
				}
				runOutput = runOutput + System.lineSeparator() + stackToString(runException);
			}
			taskResult.setRunOutput(runOutput);
			taskResult.setRunException(runException);
		} catch (IOException | InterruptedException e) {
			taskResult.setRunException(e);
			taskResult.setRunOutput(stackToString(e));
		}
	}

	private String stackToString(Exception e) {
		StringWriter writer = new StringWriter();
		e.printStackTrace(new PrintWriter(writer));
		return writer.toString();
	}

	private String fullClassName(TaskCheckRequest taskCheckRequest) {
		return taskCheckRequest.getPackageName()
				+ "."
				+ taskCheckRequest.getFileName().replace(".java", "");
	}
}
