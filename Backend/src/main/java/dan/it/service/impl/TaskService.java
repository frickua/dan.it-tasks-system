package dan.it.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

import dan.it.Language;
import dan.it.dto.TaskCheckRequest;
import dan.it.entity.TaskEntity;
import dan.it.entity.TaskResultEntity;
import dan.it.repository.ITaskRepository;
import dan.it.service.IJavaTaskExecutionService;
import dan.it.service.ITaskService;

@Component
public class TaskService implements ITaskService {

	@Autowired
	private ITaskRepository taskRepository;

	@Autowired
	private IJavaTaskExecutionService javaTaskExecutionService;

	@Override
	public List<TaskEntity> getTasks() {
		return taskRepository.getAllTasks();
	}

	@Override
	public TaskResultEntity chekTask(Long taskId, TaskCheckRequest taskCheckRequest) {
		TaskEntity task = taskRepository.getTaskById(taskId);
		if (task == null) {
			throw new IllegalArgumentException("Task " + taskId + " not found.");
		}
		if (Language.JAVA == task.getLanguage()) {
			try {
				return javaTaskExecutionService.runClass(taskCheckRequest, null);
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			throw new UnsupportedOperationException(task.getLanguage() + " not implemented yet.");
		}
		return null;
	}
}
