package dan.it.actions;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.psi.PsiJavaFile;


import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class CheckTaskAction extends AnAction {
	public CheckTaskAction() {
		super("Check task");
	}

	@Override
	public void actionPerformed(AnActionEvent anActionEvent) {

		Project project = anActionEvent.getData(PlatformDataKeys.PROJECT);

		CloseableHttpClient httpclient = HttpClients.createDefault();
		HttpPost post = new HttpPost("http://localhost:8080/tasks/1/check");
		post.setHeader("Content-Type","application/json");
		String result = null;
		try {
			PsiJavaFile javaFile = ((PsiJavaFile) anActionEvent.getData(LangDataKeys.PSI_FILE).getContainingFile());
			Map<String, Object> map = new HashMap<>();
			map.put("code", anActionEvent.getData(CommonDataKeys.EDITOR).getDocument().getText());
			map.put("packageName", javaFile.getPackageName());
			map.put("fileName", javaFile.getName());

			HttpEntity entity = new StringEntity(new ObjectMapper().writeValueAsString(map));
			post.setEntity(entity);
			CloseableHttpResponse response = httpclient.execute(post);
			try {
//				System.out.println("----------------------------------------");
//				System.out.println(response.getStatusLine());
				result = EntityUtils.toString(response.getEntity());
//				System.out.println(result);
			} finally {
				response.close();
			}
		} catch (IOException  e) {
			e.printStackTrace();
			result = e.getMessage();
		} finally {
			try {
				httpclient.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		anActionEvent.getData(CommonDataKeys.EDITOR).getProject().getProjectFile();
		((PsiJavaFile) anActionEvent.getData(LangDataKeys.PSI_FILE).getContainingFile()).getPackageName();
		Messages.showMessageDialog(project, result,
				"Your result", Messages.getInformationIcon());
	}
}
