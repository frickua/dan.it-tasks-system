package dan.it.actions;


import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;

public class SignInAction extends AnAction {
	public SignInAction() {
		super("Sign in");
	}

	@Override
	public void actionPerformed(AnActionEvent anActionEvent) {
		Project project = anActionEvent.getData(PlatformDataKeys.PROJECT);
		String answer = Messages.showInputDialog(project, "Your login:", "Input your login please", Messages.getQuestionIcon());
		Messages.showMessageDialog(project, "Hello, " + answer + "!\n", "DAN.IT", Messages.getInformationIcon());
	}
}
